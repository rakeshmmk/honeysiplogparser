#! /usr/bin/perl 
use strict;
use Data::Dumper;
use DBI;
use warnings;

use constant SIP_MSG_INIT	=> 0;
use constant SIP_MSG_START	=> 1;
use constant SIP_MSG_FOUND	=> 2;
use constant SIP_MSG_END	=> 3;


use constant SIP_MSG_FLAG_CID	=> 1;



my $LogFile		= "Logs/Merged_sip_log.log";
my $OutputFile		= "Mergeed_Output.txt";

my $LogFH;
my $OutputFH;

my $numSIPMsgs		= 0;
my %UniqueIPs;
my %UserAgents;
my %SIPMsgs;
my $SIPMsgFSM		= SIP_MSG_INIT;
my $SIPMsgFlag		= 0;
my %SIPCalls;
my ($SIPMsgType,$SIPMsgCallID);
my @TempSIPMsgType;
my $TempPattern;
my %SIPMsgPatterns;


my $temp		= 0;


open ($LogFH,"<$LogFile") or die $!;
open ($OutputFH,">$OutputFile") or die $!;


while(<$LogFH>){

	#print $_;
#	if($_=~/^(Source :)([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)(:)([0-9]+)/){
	if($_=~/^(Source :\s+)(.+)(:)(.+)/){
		
		$SIPMsgFSM	= SIP_MSG_START;
		#print "IP:$2 Port:$4\n";
		if($UniqueIPs{$2}){

			$UniqueIPs{$2}++;
		}
		else{

			$UniqueIPs{$2} 		= 1;

		}
		$numSIPMsgs++;		
	}
	if(($_=~m/(^INVITE|^CANCEL|^ACK|^REGISTER|^OPTIONS|^BYE)(\s+sip:)(.+)(@)(.+)(\s+SIP\/2.0)/ ) && ($SIPMsgFSM == SIP_MSG_START)){

		$SIPMsgFSM			= SIP_MSG_FOUND;
		$SIPMsgFlag			= 0;
		$SIPMsgType			= $1;
		if($SIPMsgs{$1}){

			$SIPMsgs{$1}++;
		}
		else{

			$SIPMsgs{$1} 		= 1;

		}
		#print " SIP Msg Found: $1 \n";
		$temp++;


	}
	if( ($_=~/(^Call-ID:\s+)(.+)/) && ($SIPMsgFSM == SIP_MSG_FOUND) ){


		$SIPMsgFlag			= $SIPMsgFlag | SIP_MSG_FLAG_CID ;
		$SIPMsgCallID			= $2;

	}
	if( ($_=~/(^User-Agent:\s+)(.+)/) && ($SIPMsgFSM == SIP_MSG_FOUND) ){


		#print "User Agent $2\n";
		if($UserAgents{$2}){
			
			$UserAgents{$2}++;
		}
		else{

			$UserAgents{$2} 	= 1;
		}

	}
	if($_=~/---------------------------------------------------/){

		#print "Found Msg End \n ";
		$SIPMsgFSM	= SIP_MSG_END;
		if($SIPMsgFlag & SIP_MSG_FLAG_CID){
				push(@{$SIPCalls{$SIPMsgCallID}},$SIPMsgType)
		}
		#$temp++;

	}

}


#print "Number of SIP Messages in the Log:$numSIPMsgs  $temp \n";
foreach my $key (keys %SIPCalls){

	$TempPattern 		= "";
	#print "\nKey $key \n\t";
	foreach(@{$SIPCalls{$key}}){
		#print "\t".$_;
		$TempPattern		= $TempPattern.",".$_;
	}
	$TempPattern		=~s/^,//s;
	if($SIPMsgPatterns{$TempPattern}){

		$SIPMsgPatterns{$TempPattern}++;
	}
	else{

		$SIPMsgPatterns{$TempPattern}		= 1;
	}
	#print $TempPattern."\n";

}
#print  Dumper \%SIPMsgPatterns;
#print Dumper \%SIPMsgs;
#print Dumper \%SIPCalls;
#print Dumper \%UniqueIPs;
#print Dumper \%UserAgents;

PrintHashValues($OutputFH,\%SIPMsgPatterns,"Detected SIP Msg Patterns","Pattern","Number of Occurances");
PrintHashValues($OutputFH,\%UniqueIPs,"Detected IPs","IPs","Number of Occurances");
PrintHashValues($OutputFH,\%UserAgents,"Detected User Agents","User Agents","Number of Occurances");

close($LogFH);
close($OutputFH);

sub PrintHashValues{

	my $OutFH			= shift;
	my $HashPtrToPrint		= shift;
	my $StringToPrint		= shift;
	my $StringForKey		= shift;
	my $StringForVal		= shift;

	print $OutputFH  "#############################################################################################\n";
	print $OutputFH  $StringToPrint."\n";
	print $OutputFH  "\t".$StringForKey."\t=> ".$StringForVal."\n";
	print $OutputFH  "\t----------------------------------------------------------------------------------------\n";
	foreach my $key (keys %$HashPtrToPrint){
		
		print 	$OutputFH  "\t".$key."\t=> ".$HashPtrToPrint->{$key}."\n";
	
	}



}

